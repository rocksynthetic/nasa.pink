"""
NASA is a band from Wisconsin

Paul Feyertag: Vocals, guitar, computer
Erik Schoster: Computer
"""
import socket
import os
import multiprocessing


with open(__file__) as src:
    body = src.read()
    html = "".join(["<{}>".format(x) for x in ["html","pre"]]) + "\n" 

def realclose(conn):
    conn.shutdown(socket.SHUT_RDWR)
    conn.close()


# <h1>drenched</h1>
def handler(conn):
    conn.send(bytes('HTTP/1.1 200 OK\r\n\r\n' + html + body, 'ascii'))
    realclose(conn)


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('', 80))
    sock.listen(100)
    
    try:
        while 1:
            client, address = sock.accept()
            conn = client.dup()
            proc = multiprocessing.Process(target=handler, args=(conn,))
            proc.start()

    finally:
        realclose(sock)


if __name__ == '__main__':
    main()
